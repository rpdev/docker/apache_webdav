# Simple WebDAV server based on Apache for Testing

This project holds a recipe and sample Docker image which provides a WebDAV service based on Apache which can be used e.g. for unit testing.

The project contains a `Dockerfile`, from which you can build the image on your own. In addition, you can use the pre-build image like this:

```bash
docker pull registry.gitlab.com/rpdev/docker/apache_webdav
docker run -it -d -p 8080:80 registry.gitlab.com/rpdev/docker/apache_webdav
```

This will spawn the Apache server and bind the local post `8080` to the port  `80` in the container. On the host, you can now access the WebDAV server via `http://localhost:8080/webdav`. The username is `admin`, the password is `admin` as well.

**Important:** This it not a "productive" WebDAV image! It is intended to be used in testing of WebDAV client implementations only.