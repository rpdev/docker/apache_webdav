FROM centos:7

RUN \
      yum install -y httpd
RUN \
      mkdir /var/www/ToDoList && \
      chown apache:apache /var/www/ToDoList && \
      mkdir -p /etc/apache2/auth/ && \
      touch /etc/apache2/auth/ToGoList_passwd && \
      htpasswd -b /etc/apache2/auth/ToGoList_passwd admin admin

ADD dav.conf /etc/httpd/conf.d/dav.conf

EXPOSE 80

ENTRYPOINT [ "httpd", "-DFOREGROUND" ]
